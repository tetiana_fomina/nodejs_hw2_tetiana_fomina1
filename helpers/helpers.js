/**
 * Throws errors.
 * @return {Object}nothing.
 * @param {Function} callback for callback call.
 */
function asyncWrapper(callback) {
  return (req, res, next) => {
    callback(req, res, next).catch(next);
  };
}

module.exports = {
  asyncWrapper,
};
