const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
module.exports.registration = async (req, res) => {
  const {username, password} = req.body;

  if (username && password) {
    let user = await User.findOne({username});
    if (user) {
      res.status(400).json({message: 'User is already exist!'});
    } else {
      user = new User({
        username,
        password: await bcrypt.hash(password, 10),
      });
      const token = jwt.sign(
          {username: user.username, _id: user._id},
          JWT_SECRET,
      );

      user.tokens = user.tokens.concat({token});

      await user.save();

      res.status(200).json({message: 'Success'});
    }
  } else {
    res.status(400).json({message: 'Enter all credentials'});
  }
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});
  if (!username && !password) {
    res.status(400).json({message: 'Enter your credentials!'});
  }

  if (!user) {
    return res
        .status(400)
        .json({message: `No user with username '${username}' found!`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign(
      {username: user.username, _id: user._id},
      JWT_SECRET,
  );

  user.tokens = user.tokens.concat({token});
  await user.save();
  res.status(200).json({message: 'Success', jwt_token: token});
};
